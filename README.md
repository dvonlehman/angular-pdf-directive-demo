Demo app showing how to load a PDF in an iframe via the s3-proxy plugin. View the app at https://angular-pdf-directive-demo.aerobatic.io.

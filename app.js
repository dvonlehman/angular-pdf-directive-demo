
angular.module('demoApp',[])
  .controller('PdfController', ['$scope', '$sce', function($scope, $sce) {
    $scope.pdfUrl = $sce.trustAsResourceUrl('/s3/demo/pdf%20sample.pdf');
  }])
  .directive('pdf', ['$sce', function($sce) {
    return {
      replace: true,
      template: function(elem, attr) {
        return '<iframe src="' + attr.url + '" style="width:100%;height:100%">' +
          '<object data="' + attr.url + '" type="application/pdf" style="width:100%;height:100%" />' +
          '<embed src="' + attr.url + '" type="application/pdf" style="width:100%;height:100%" />'
          '</iframe>';
      }
    };
  }]);
